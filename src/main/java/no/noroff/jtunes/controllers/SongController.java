package no.noroff.jtunes.controllers;

import no.noroff.jtunes.models.SongSearch;
import no.noroff.jtunes.repository.SongRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;


@Controller
@RequestMapping()
public class SongController {


    @Autowired
    private SongRepository songRepository;

    @GetMapping
    public String index(Model model) {
        model.addAttribute("songSearch", new SongSearch());
        model.addAttribute("songs", songRepository.getRandomSongs(5));
        return "view-songs";
    }

    @GetMapping("search")
    public String showSongBySearch(@RequestParam("searchValue") String searchQuery, Model model) {
        model.addAttribute("songs", songRepository.getByName(searchQuery.toLowerCase()));
        return "song-search";
    }

}
