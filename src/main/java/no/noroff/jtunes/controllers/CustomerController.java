package no.noroff.jtunes.controllers;

import no.noroff.jtunes.models.Country;
import no.noroff.jtunes.models.Customer;
import no.noroff.jtunes.models.CustomerGenre;
import no.noroff.jtunes.models.CustomerSpender;
import no.noroff.jtunes.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/customers")
public class CustomerController {

    @Autowired
    private CustomerRepository customerRepository;

    @GetMapping
    public List<Customer> getAll() {
        return customerRepository.getAll();
    }

    @GetMapping("{id}")
    public Customer getById(@PathVariable String id) {
        return customerRepository.getById(id);
    }

    @GetMapping(value = "search", params = { "name" })
    public List<Customer> getByName(@RequestParam(value = "name") String name) {
        return customerRepository.getByName(name);
    }

    @GetMapping(value = "search", params = { "firstname", "lastname" })
    public List<Customer> getByFirstNameLastName(@RequestParam(value = "firstname") String firstName,
                                                 @RequestParam(value = "lastname") String lastName) {
        return customerRepository.getByName(firstName, lastName);
    }

    @PostMapping
    public int add(@RequestBody Customer customer) {
        return customerRepository.add(customer);
    }

    @PutMapping("{id}")
    public int update(@PathVariable String id, @RequestBody Customer customer) {
        if (Integer.parseInt(id) == customer.customerId()) {
            return customerRepository.update(customer);
        }
        return 0;
    }

    @GetMapping("page")
    public List<Customer> getByPage(@RequestParam("page") int page,
                                    @RequestParam(value = "pagesize", required = false, defaultValue = "10") int pageSize) {
        return customerRepository.getByPage(page, pageSize);
    }

    @GetMapping("countries")
    public List<Country> getCountries() {
        return customerRepository.getCountries();
    }

    @GetMapping("spenders")
    public List<CustomerSpender> getSpenders() {
        return customerRepository.getSpenders();
    }

    @GetMapping("{id}/genres")
    public List<CustomerGenre> getGenres(@PathVariable String id) {
        return customerRepository.getGenres(customerRepository.getById(id));
    }
}
