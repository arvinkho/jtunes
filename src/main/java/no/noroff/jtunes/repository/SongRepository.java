package no.noroff.jtunes.repository;

import no.noroff.jtunes.models.Song;
import no.noroff.jtunes.util.SqlLiteConnectionHelper;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Repository
public class SongRepository implements IRepository<Song>{

    /**
     * Gathers information about a specific song by track id.
     * Lists information such as track name, genre, artist name and album id.
     * @param id track id
     * @return song object
     */
    @Override
    public Song getById(String id) {
        String sql = "SELECT Track.name AS trackName, Track.TrackId AS track_id, " +
                "Genre.name AS genreName, Track.albumid AS album_id, Artist.artistid, Artist.name AS artistName " +
                "FROM Track " +
                "INNER JOIN Album on Track.albumid = Album.albumid " +
                "INNER JOIN Artist on Album.artistid = Artist.artistid " +
                "INNER JOIN Genre on Genre.GenreId = Track.GenreId" +
                "WHERE track.trackid = ?";
        try (Connection conn = DriverManager.getConnection(SqlLiteConnectionHelper.getConnectionString())) {
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1, id);
            ResultSet result = statement.executeQuery();
            return new Song(
                    result.getString("trackName"),
                    result.getString("artistName"),
                    result.getString("genreName"),
                    result.getInt("track_id"),
                    result.getInt("album_id"));
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }


    /**
     * Get information about n random songs.
     * @param numOfSongs number of songs to get information about
     * @return List of songs
     */
    public List<Song> getRandomSongs(int numOfSongs) {

        List<Song> songs = new ArrayList<>();

        String sql = "SELECT Track.name AS trackName, Track.TrackId AS track_id, " +
                "Genre.name AS genreName, Track.albumid AS album_id, Artist.artistid, Artist.name AS artistName " +
                "FROM Track " +
                "INNER JOIN Album on Track.albumid = Album.albumid " +
                "INNER JOIN Artist on Album.artistid = Artist.artistid " +
                "INNER JOIN Genre on Genre.GenreId = Track.GenreId " +
                "ORDER BY RANDOM() LIMIT ?";

        try (Connection conn = DriverManager.getConnection(SqlLiteConnectionHelper.getConnectionString())) {
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1, numOfSongs);
            ResultSet result = statement.executeQuery();
            while (result.next()) {
                Song song = new Song(
                        result.getString("trackName"),
                        result.getString("artistName"),
                        result.getString("genreName"),
                        result.getInt("track_id"),
                        result.getInt("album_id"));
                songs.add(song);

            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return songs;
    }


    /**
     * Search for a song. Returns a list of song that matches the search query. (Title or artist name)
     * @param searchQuery
     * @return songs matching the search query
     */
    public List<Song> getByName(String searchQuery) {
        List<Song> songs = new ArrayList<>();
        String sql = "SELECT Track.name AS trackName, Track.TrackId AS track_id, " +
                "Genre.name AS genreName, Track.albumid AS album_id, Artist.artistid, Artist.name AS artistName " +
                "FROM Track " +
                "INNER JOIN Album on Track.albumid = Album.albumid " +
                "INNER JOIN Artist on Album.artistid = Artist.artistid " +
                "INNER JOIN Genre on Genre.GenreId = Track.GenreId " +
                "WHERE trackName LIKE ?" +
                "OR artistName LIKE ?";
        try (Connection conn = DriverManager.getConnection(SqlLiteConnectionHelper.getConnectionString())) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1, "%" + searchQuery + "%");
            statement.setString(2, "%" + searchQuery + "%");
            // Execute
            ResultSet result = statement.executeQuery();
            // Handle results

            while (result.next()) {
                Song song = new Song(
                        result.getString("trackName"),
                        result.getString("artistName"),
                        result.getString("genreName"),
                        result.getInt("track_id"),
                        result.getInt("album_id")
                );

                songs.add(song);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return songs;
    }

    /**
     * Returns information about all songs, such as track name, track id, genre name,
     * album id and artist name
     * @return list of all songs in the db
     */
    @Override
    public List<Song> getAll() {
        List<Song> songs = new ArrayList<>();

        String sql = "SELECT Track.name AS trackName, Track.TrackId AS track_id, " +
                "Genre.name AS genreName, Track.albumid AS album_id, Artist.artistid, Artist.name AS artistName " +
                "FROM Track " +
                "INNER JOIN Album on Track.albumid = Album.albumid " +
                "INNER JOIN Artist on Album.artistid = Artist.artistid " +
                "INNER JOIN Genre on Genre.GenreId = Track.GenreId";

        try (Connection conn = DriverManager.getConnection(SqlLiteConnectionHelper.getConnectionString())) {
            PreparedStatement statement = conn.prepareStatement(sql);
            ResultSet result = statement.executeQuery();
            while (result.next()) {
                Song song = new Song(
                        result.getString("trackName"),
                        result.getString("artistName"),
                        result.getString("genreName"),
                        result.getInt("track_id"),
                        result.getInt("album_id"));
                songs.add(song);

            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return songs;
    }

    @Override
    public int save(Song song) {
        return 0;
    }

    @Override
    public int update(Song song) {
        return 0;
    }

    @Override
    public int delete(Song song) {
        return 0;
    }

    @Override
    public int add(Song song) {
        return 0;
    }
}
