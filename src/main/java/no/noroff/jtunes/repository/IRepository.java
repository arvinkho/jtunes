package no.noroff.jtunes.repository;

import java.util.List;

public interface IRepository<T> {

    T getById(String id);

    List<T> getAll();

    int save(T t);

    int update(T t);

    int delete(T t);

    int add(T t);
}
