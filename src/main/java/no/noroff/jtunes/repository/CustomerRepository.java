package no.noroff.jtunes.repository;

import no.noroff.jtunes.models.Country;
import no.noroff.jtunes.models.Customer;
import no.noroff.jtunes.models.CustomerGenre;
import no.noroff.jtunes.models.CustomerSpender;
import no.noroff.jtunes.util.SqlLiteConnectionHelper;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@Repository
public class CustomerRepository implements IRepository<Customer>{


    /**
     * Gathers all present customers from the JDBC. Creates a SQL query as a string,
     * and then executes this query on the DB. Returns a List containing all present customers in the DB.
     * @return all customers in the DB
     */
    @Override
    public List<Customer> getAll() {
        List<Customer> customers = new ArrayList<>();
        String sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer";
        try (Connection conn = DriverManager.getConnection(SqlLiteConnectionHelper.getConnectionString())) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            // Execute
            ResultSet result = statement.executeQuery();
            // Handle results
            while (result.next()) {
                Customer customer = new Customer(
                        result.getInt("CustomerId"),
                        result.getString("FirstName"),
                        result.getString("LastName"),
                        result.getString("Country"),
                        result.getString("PostalCode"),
                        result.getString("Phone"),
                        result.getString("Email")
                );
                customers.add(customer);
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return customers;
    }

    /**
     * Gathers all present customers from the JDBC, sorted in subsets of given amount. Creates a SQL query as a string and takes
     * page number as an argument, and then executes this query on the DB.
     * Returns a List containing all present customers in the DB on the desired "page".
     * @return all customers in the DB page
     */
    public List<Customer> getByPage(int page, int pageSize) {
        List<Customer> customers = new ArrayList<>();
        String sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer " +
                "LIMIT ? OFFSET ?";
        try (Connection conn = DriverManager.getConnection(SqlLiteConnectionHelper.getConnectionString())) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1, pageSize);
            statement.setInt(2, (page - 1) * pageSize);
            // Execute
            ResultSet result = statement.executeQuery();
            // Handle results
            while (result.next()) {
                Customer customer = new Customer(
                        result.getInt("CustomerId"),
                        result.getString("FirstName"),
                        result.getString("LastName"),
                        result.getString("Country"),
                        result.getString("PostalCode"),
                        result.getString("Phone"),
                        result.getString("Email")
                );
                customers.add(customer);
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return customers;
    }

    /**
     * Gathers and returns a single customer from the JDBC based on the customer id supplied.
     * Creates a SQL query as a string, modifies this query with the supplied customer id,
     * and executes the query.
     * @return customer with corresponding customer id
     */
    @Override
    public Customer getById(String id) {
        String sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email " +
                "FROM Customer WHERE CustomerId = ?";
        try (Connection conn = DriverManager.getConnection(SqlLiteConnectionHelper.getConnectionString())) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1, id);
            // Execute
            ResultSet result = statement.executeQuery();
            // Handle results
            return new Customer(
                    result.getInt("CustomerId"),
                    result.getString("FirstName"),
                    result.getString("LastName"),
                    result.getString("Country"),
                    result.getString("PostalCode"),
                    result.getString("Phone"),
                    result.getString("Email")
            );
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }


    /**
     * Gathers and returns all customers which contains the supplied name from the JDBC as a List.
     * Creates a SQL query as a string, modifies this query with the supplied name, and
     * executes the query.
     * @param name the name to search for
     * @return the customer(s) as List
     */
    public List<Customer> getByName(String name) {
        List<Customer> customers = new ArrayList<>();
        String sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email " +
                "FROM Customer WHERE FirstName LIKE ? OR LastName LIKE ?";
        try (Connection conn = DriverManager.getConnection(SqlLiteConnectionHelper.getConnectionString())) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1, "%" + name + "%");
            statement.setString(2, "%" + name + "%");
            // Execute
            ResultSet result = statement.executeQuery();
            // Handle results

            while (result.next()) {
                Customer customer = new Customer(
                        result.getInt("CustomerId"),
                        result.getString("FirstName"),
                        result.getString("LastName"),
                        result.getString("Country"),
                        result.getString("PostalCode"),
                        result.getString("Phone"),
                        result.getString("Email")
                );
                customers.add(customer);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return customers;
    }

    /**
     * Gathers and returns all customers which contains the supplied name from the JDBC as a List.
     * Creates a SQL query as a string, modifies this query with the supplied name, and
     * executes the query.
     * @param firstName the firstname to search for
     * @param lastName the lastname to search for
     * @return the customer(s) as List
     */
    public List<Customer> getByName(String firstName, String lastName) {
        List<Customer> customers = new ArrayList<>();
        String sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email " +
                "FROM Customer WHERE FirstName LIKE ? OR LastName LIKE ?";
        try (Connection conn = DriverManager.getConnection(SqlLiteConnectionHelper.getConnectionString())) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1, "%" + firstName + "%");
            statement.setString(2, "%" + lastName + "%");
            // Execute
            ResultSet result = statement.executeQuery();
            // Handle results

            while (result.next()) {
                Customer customer = new Customer(
                        result.getInt("CustomerId"),
                        result.getString("FirstName"),
                        result.getString("LastName"),
                        result.getString("Country"),
                        result.getString("PostalCode"),
                        result.getString("Phone"),
                        result.getString("Email")
                );
                customers.add(customer);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return customers;
    }



    @Override
    public int save(Customer customer) {
        return 0;
    }

    @Override
    public int update(Customer customer) {
        int result = 0;
        try(Connection conn = DriverManager.getConnection(SqlLiteConnectionHelper.getConnectionString())) {
            // Create SQL query
            PreparedStatement preparedStatement =
                    conn.prepareStatement("UPDATE Customer " +
                            "SET  FirstName = ?, LastName = ?, Country = ?, PostalCode = ?, " +
                            "Phone = ?, Email = ? " +
                            "WHERE CustomerId = ?" );

            preparedStatement.setString(1, customer.firstName());
            preparedStatement.setString(2, customer.lastName());
            preparedStatement.setString(3, customer.country());
            preparedStatement.setString(4, customer.postalCode());
            preparedStatement.setString(5, customer.phone());
            preparedStatement.setString(6, customer.email());
            preparedStatement.setInt(7, customer.customerId());

            //Execute query
            result = preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return result;
    }


    @Override
    public int delete(Customer customer) {
        return 0;
    }


    /**
     * Adds a customer to the DB.
     * @param customer customer to add
     * @return response code
     */
    @Override
    public int add(Customer customer) {
        int result = 0;
        try(Connection conn = DriverManager.getConnection(SqlLiteConnectionHelper.getConnectionString())) {
            // Create SQL query
            PreparedStatement preparedStatement =
                    conn.prepareStatement("INSERT INTO customer(customerId, FirstName, LastName, Country, PostalCode, " +
                            "Phone, Email) VALUES (?, ?, ?, ?, ?, ?, ?)");
            preparedStatement.setInt(1, customer.customerId());
            preparedStatement.setString(2, customer.firstName());
            preparedStatement.setString(3, customer.lastName());
            preparedStatement.setString(4, customer.country());
            preparedStatement.setString(5, customer.postalCode());
            preparedStatement.setString(6, customer.phone());
            preparedStatement.setString(7, customer.email());
            //Execute query
            result = preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return result;
    }


    /**
     * Gathers and returns all countries represented by customers from the JDBC as a list of countries,
     * sorted in descending order by number of customers. Creates a SQL query as a string and executes it.
     * @return list of countries
     */
    public List<Country> getCountries() {
        List<Country> countries = new ArrayList<>();
        String sql = "SELECT country, COUNT(country) AS customers FROM customer GROUP BY country ORDER BY customers DESC";
        try(Connection conn = DriverManager.getConnection(SqlLiteConnectionHelper.getConnectionString())) {
            PreparedStatement statement = conn.prepareStatement(sql);
            ResultSet result = statement.executeQuery();
            while (result.next()) {
                Country country = new Country(result.getString("Country"), result.getInt("customers"));
                countries.add(country);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return countries;
    }

    /**
     * Gathers and returns all spenders represented from the JDBC as a list of spenders,
     * sorted in descending order by total spent. Creates a SQL query as a string and executes it.
     * @return list of spenders
     */
    public List<CustomerSpender> getSpenders() {
        List<CustomerSpender> spenders = new ArrayList<>();
        String sql = "SELECT customerid, SUM(total) as sum_total FROM Invoice GROUP BY customerid ORDER BY sum_total DESC\n";
        try (Connection conn = DriverManager.getConnection(SqlLiteConnectionHelper.getConnectionString())) {
            PreparedStatement statement = conn.prepareStatement(sql);
            ResultSet result = statement.executeQuery();
            while (result.next()) {
                CustomerSpender spender = new CustomerSpender(result.getInt("customerId"),
                        result.getDouble("sum_total"));
                spenders.add(spender);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return spenders;
    }


    /**
     * Gathers all the represented genres based on a customers purchased tracks. Finds the most popular
     * genre, and returns it. If several genres have the same amount of tracks, all the genres with
     * the maximum amount of tracks will be returned.
     * @param customer customer to find the most popular genre of
     * @return the most popular genres
     */
    public List<CustomerGenre> getGenres(Customer customer) {
        List<CustomerGenre> genres = new LinkedList<>();
        String sql = "SELECT Genre.name, genre.genreid, customerid, COUNT(genre.genreid) as cnt " +
                "FROM Genre " +
                "INNER JOIN Track ON Genre.GenreId = Track.GenreId " +
                "INNER JOIN InvoiceLine ON InvoiceLine.TrackId = Track.TrackId " +
                "INNER JOIN Invoice ON InvoiceLine.invoiceid = Invoice.invoiceid " +
                "WHERE Invoice.customerid = ? GROUP BY Genre.genreid ORDER BY cnt DESC";

        try (Connection conn = DriverManager.getConnection(SqlLiteConnectionHelper.getConnectionString())) {
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1, customer.customerId());
            ResultSet results = statement.executeQuery();
            int maxCount = 0;
            while (results.next()) {
                int currentCount = results.getInt("cnt");
                CustomerGenre genre = new CustomerGenre(results.getString("Name"),
                        results.getInt("genreId"),
                        results.getInt("cnt"));
                System.out.println(currentCount);
                if (currentCount > maxCount) {
                    genres.clear();
                    genres.add(genre);
                    maxCount = currentCount;
                } else if (currentCount == maxCount) {
                    genres.add(genre);
                } else {
                    break;
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return genres;
    }

}
