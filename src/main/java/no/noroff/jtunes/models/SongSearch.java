package no.noroff.jtunes.models;

public class SongSearch {
    private String searchValue = "";

    public SongSearch() {
    }

    public SongSearch(String searchValue) {
        this.searchValue = searchValue;
    }

    public String getSearchValue() {
        return searchValue;
    }

    public void setSearchValue(String searchValue) {
        this.searchValue = searchValue;
    }
}
