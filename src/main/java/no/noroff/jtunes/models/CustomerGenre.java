package no.noroff.jtunes.models;

public class CustomerGenre {
    private String genreName;
    private int genreId;
    private int genreCount;

    public CustomerGenre(String genreName, int genreId, int genreCount) {
        this.genreName = genreName;
        this.genreId = genreId;
        this.genreCount = genreCount;
    }


    public String getGenreName() {
        return genreName;
    }

    public void setGenreName(String genreName) {
        this.genreName = genreName;
    }

    public int getGenreId() {
        return genreId;
    }

    public void setGenreId(int genreId) {
        this.genreId = genreId;
    }

    public int getGenreCount() {
        return genreCount;
    }

    public void setGenreCount(int genreCount) {
        this.genreCount = genreCount;
    }

}
