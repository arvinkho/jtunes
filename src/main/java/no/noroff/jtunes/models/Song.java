package no.noroff.jtunes.models;

public class Song {

    private String songName;
    private String artistName;
    private String genre;
    private int trackId;
    private int albumId;

    public Song(String songName, String artistName, String genre, int trackId, int albumId) {
        this.songName = songName;
        this.artistName = artistName;
        this.genre = genre;
        this.trackId = trackId;
        this.albumId = albumId;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getSongName() {
        return songName;
    }

    public void setSongName(String songName) {
        this.songName = songName;
    }

    public String getArtistName() {
        return artistName;
    }

    public void setArtistName(String artistName) {
        this.artistName = artistName;
    }

    public int getTrackId() {
        return trackId;
    }

    public void setTrackId(int trackId) {
        this.trackId = trackId;
    }

    public int getAlbumId() {
        return albumId;
    }

    public void setAlbumId(int albumId) {
        this.albumId = albumId;
    }
}
