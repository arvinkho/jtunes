package no.noroff.jtunes.models;

public class Country {
    private String countryName;
    private int customerCount;

    public Country(String countryName, int customerCount) {
        this.countryName = countryName;
        this.customerCount = customerCount;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public int getCustomerCount() {
        return customerCount;
    }

    public void setCustomerCount(int customerCount) {
        this.customerCount = customerCount;
    }
}
