package no.noroff.jtunes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JtunesApplication {

	public static void main(String[] args) {
		SpringApplication.run(JtunesApplication.class, args);
	}

}
