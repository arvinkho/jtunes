# Background
This is a console Java application for the second assignment in the Accelerate course.

It is an intro to Spring Boot, SQL-queries from java and Thymeleaf support. The project is dockerized and pushed to Heroku: https://arvinkho-jtunes.herokuapp.com

It covers some design patterns and principles, such as the Repository-pattern. 

# Install
* Install JDK 17
* Install IntelliJ
* Clone repo

# Usage
When arriving at the home page, you are greeted with five random songs, with the belonging artists and genres. You can search in the search bar, this will show matches in artists and songs.


This application is a simple representation of a music library. (Totally not getting inspiration from other, famous music libraries)
The API supports listing of:
* All customers
* A specific customer by ID
* A customer(s) by name
* Customers by pages (page size and page number can be selected)
* List of countries represented by number of customers, in descending order
* List of how much each customer has spent in the store, in descending order
* Most popular genre for each customer (by customer ID)

It also supports addition of new customers.


